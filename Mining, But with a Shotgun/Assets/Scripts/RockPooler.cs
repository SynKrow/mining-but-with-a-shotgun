﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockPooler : MonoBehaviour {

    public static RockPooler rockPooler;
    [SerializeField] private GameObject gRock;
    [SerializeField] private int nRockAmount;


    [SerializeField] private bool bSpawn;
    private List<GameObject> lPooledRocks = new List<GameObject>();

    private void Awake()
    {
        rockPooler = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < nRockAmount; i++)
        {
            GameObject gRocks = (GameObject)Instantiate(gRock);
            lPooledRocks.Add(gRocks);
            gRocks.SetActive(false);
        }
    }

    public GameObject GetRock()
    {
        for (int i = 0; i < lPooledRocks.Count; i++)
        {
            if (!lPooledRocks[i].activeInHierarchy)
            {
                return lPooledRocks[i];
            }
        }
        if (bSpawn)
        {
            GameObject gRocks = (GameObject)Instantiate(gRock);
            lPooledRocks.Add(gRocks);
            return gRocks;
        }
        return null;
    }

    public void IncreaseRate()
    {
        for(int i = 0; i < lPooledRocks.Count; i++)
        {
            lPooledRocks[i].GetComponent<MainRock>().IncreaseRate();
        }
    }

}
