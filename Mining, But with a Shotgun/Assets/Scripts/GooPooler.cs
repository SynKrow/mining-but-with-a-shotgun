﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooPooler : MonoBehaviour {

    public static GooPooler gooPooler;
    [SerializeField] private GameObject gooGO;
    [SerializeField] private int nGooAmount;

    [SerializeField] private GameObject rockGO;
    [SerializeField] private int nRockAmount;

    [SerializeField] private bool bSpawn;
    private List<GameObject> lGooAmount = new List<GameObject>();
    private List<GameObject> lRockAmount = new List<GameObject>();

    private void Awake()
    {
        gooPooler = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < nGooAmount; i++)
        {
            GameObject gGoo = (GameObject)Instantiate(gooGO);
            lGooAmount.Add(gGoo);
            gGoo.SetActive(false);
        }
        for (int i = 0; i < nRockAmount; i++)
        {
            GameObject gRock = (GameObject)Instantiate(rockGO);
            lRockAmount.Add(gRock);
            gRock.SetActive(false);
        }
    }

    public GameObject GetGoo()
    {
        for (int i = 0; i < lGooAmount.Count; i++)
        {
            if (!lGooAmount[i].activeInHierarchy)
            {
                return lGooAmount[i];
            }
        }
        if (bSpawn)
        {
            GameObject gGoo = (GameObject)Instantiate(gooGO);
            lGooAmount.Add(gGoo);
            return gGoo;
        }
        return null;
    }
    public GameObject GetRock()
    {
        for (int i = 0; i < lRockAmount.Count; i++)
        {
            if (!lRockAmount[i].activeInHierarchy)
            {
                return lRockAmount[i];
            }
        }
        if (bSpawn)
        {
            GameObject gRock = (GameObject)Instantiate(rockGO);
            lRockAmount.Add(gRock);
            return gRock;
        }
        return null;
    }
}
