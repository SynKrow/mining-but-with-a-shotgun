﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class HighScoreHolder : MonoBehaviour {

    public static HighScoreHolder hsInstance { get; private set; }
    private int nHighScore;
    public int HighScore
    {
        get { return nHighScore; }
        set { nHighScore = value; }
    }

    private bool bPlayOnAwake;
    public bool PlayOnAwakeBool
    {
        get { return bPlayOnAwake; }
        set { bPlayOnAwake = value; }
    }

    private void Awake()
    {
        if (hsInstance == null)
        {
            hsInstance = this;
            DontDestroyOnLoad(gameObject);
            Load();
        }
        else
            Destroy(gameObject);
        Invoke("AwakenTheNoise", .05f);
    }

    private void AwakenTheNoise()
    {
        bPlayOnAwake = true;
    }

    private float fGS = .7f;
    public float GameSound
    {
        get { return fGS; }
        set { fGS = value; }
    }

    private float fMusic = .4f;
    public float Music
    {
        get { return fMusic; }
        set { fMusic = value; }
    }

    public void SetHighScore(int score)
    {
        if (score > nHighScore)
            nHighScore = score;
    }
    private bool bAutoSkip;
    public bool AutoSkip
    {
        get { return bAutoSkip; }
        set { bAutoSkip = value; }
    }

    public void Saves()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SuperSecretFileLolDoNotOpen.dat");

        ScoreData score = new ScoreData();
        score.nPlayScore = nHighScore;
        score.bTutSkip = bAutoSkip;
        score.fGSounds = fGS;
        score.fMus = fMusic;

        bf.Serialize(file, score);
        file.Close();
    }



    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/SuperSecretFileLolDoNotOpen.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/SuperSecretFileLolDoNotOpen.dat", FileMode.Open);
            ScoreData score = (ScoreData)bf.Deserialize(file);

            nHighScore = score.nPlayScore;
            bAutoSkip = score.bTutSkip;
            fGS = score.fGSounds;
            fMusic = score.fMus;
        }
    }
}

[Serializable]
class ScoreData
{
    public int nPlayScore;
    public float fGSounds;
    public float fMus;
    public bool bTutSkip;
}