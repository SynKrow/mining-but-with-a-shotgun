﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooDisabler : MonoBehaviour {

    private float fTimer;

    private void OnEnable()
    {
        fTimer = 0;
    }
	
	// Update is called once per frame
	void Update () {
        fTimer += Time.deltaTime;
        if(fTimer >= .5)
        {
            gameObject.SetActive(false);
        }
	}
}
