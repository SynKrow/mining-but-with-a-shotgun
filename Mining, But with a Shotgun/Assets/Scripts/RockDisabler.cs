﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockDisabler : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MainRock>())
            other.GetComponent<MainRock>().gameObject.SetActive(false);
    }
}
