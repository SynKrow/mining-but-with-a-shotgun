﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainRock : MonoBehaviour
{

    private bool bCritical;
    private float fTimer;
    private int nMaxHealth = 2;
    private int nCurHealth;
    private float fDropRate = .35f;
    [SerializeField] private Sprite[] gSprite;
    [SerializeField] private GameObject curSprite;

    private bool bDamagable = true;
    public bool Damagable
    {
        get { return bDamagable; }
        set { bDamagable = value; }
    }

    // Use this for initialization
    void OnEnable()
    {
        nCurHealth = 0;
        curSprite.GetComponent<SpriteRenderer>().sprite = gSprite[nCurHealth];
    }

    public void Damaged(int dmg)
    {
        OneShotPlaylist.oInstance.RockCrush();
        nCurHealth = nCurHealth + dmg;
        if (nCurHealth >= nMaxHealth)
        {
            gameObject.SetActive(false);
            Drop();
        }
        else
        {
            curSprite.GetComponent<SpriteRenderer>().sprite = gSprite[nCurHealth];
        }
    }

    private void Drop()
    {
        if (Random.Range(0, 1.01f) <= fDropRate)
        {
            GameObject gAmmo = ItemPooler.itemPooler.GetAmmo();
            gAmmo.transform.position = transform.position;
            gAmmo.SetActive(true);
        }
        if (Random.Range(0, 1.01f) >= .91f)
        {
            GameObject gHealth = ItemPooler.itemPooler.GetHealth();
            gHealth.transform.position = transform.position;
            gHealth.SetActive(true);
        }
        GameObject gRock = GooPooler.gooPooler.GetRock();
        gRock.transform.position = new Vector3(transform.position.x, transform.position.y + .5f, transform.position.z);
        gRock.SetActive(true);
    }

    public void IncreaseRate()
    {
        if(fDropRate <= .85f)
        {
            fDropRate = fDropRate + .002f;
        }
    }
}
