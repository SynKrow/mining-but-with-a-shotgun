﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour {

    public static BulletPooler bulletPooler;
    [SerializeField] private GameObject gShotgunPellet;
    [SerializeField] private int nPelletAmount;


    [SerializeField] private bool bSpawn;
    private List<GameObject> lPooledPellets = new List<GameObject>();

    private void Awake()
    {
        bulletPooler = this;
    }
    // Use this for initialization
    void Start () {
        for(int i = 0; i < nPelletAmount; i++)
        {
            GameObject gPellet = (GameObject)Instantiate(gShotgunPellet);
            lPooledPellets.Add(gPellet);
            gPellet.SetActive(false);
        }
	}
	
	public GameObject GetPellet()
    {
        for(int i = 0; i < lPooledPellets.Count; i++)
        {
            if (!lPooledPellets[i].activeInHierarchy)
            {
                return lPooledPellets[i];
            }
        }
        if (bSpawn)
        {
            GameObject gPellet = (GameObject)Instantiate(gShotgunPellet);
            lPooledPellets.Add(gPellet);
            return gPellet;
        }
        return null;
    }
}
