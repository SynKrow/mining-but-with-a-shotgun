﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {


    [SerializeField] private bool bHealth;
    [SerializeField] private bool bAmmo;
    private PlayerData pData;

    private float fTimer;

    private void OnEnable()
    {
        fTimer = 0;
    }

    private void Update()
    {
        fTimer += Time.deltaTime;
        if (fTimer > 10)
            gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerData>())
        {
            if (bHealth)
                PlayerData.pData.HealthPickUp();
            if (bAmmo)
                PlayerData.pData.AmmoPickUp();
            OneShotPlaylist.oInstance.PlayHealPickUp();
            gameObject.SetActive(false);
        }
        if (other.GetComponent<EnemyData>())
        {
            if (bAmmo)
                other.GetComponent<EnemyData>().Heal();
            gameObject.SetActive(false);
        }
    }
}
