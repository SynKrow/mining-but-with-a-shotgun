﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotPlaylist : MonoBehaviour {

    public static OneShotPlaylist oInstance;
    private AudioSource aSource;
    [SerializeField] private AudioSource aMusic;
    //[SerializeField] private AudioClip aFriendship;

    [SerializeField] private AudioClip menu1;
    [SerializeField] private AudioClip menu2;
    [SerializeField] private AudioClip gunshot;
    [SerializeField] private AudioClip ectoshot;
    [SerializeField] private AudioClip pause;
    [SerializeField] private AudioClip waveque;
    [SerializeField] private AudioClip[] characterdamage;
    [SerializeField] private AudioClip[] zombiedamage;
    [SerializeField] private AudioClip[] zombiegroan;
    [SerializeField] private AudioClip healpickup;
    [SerializeField] private AudioClip menumusic;
    [SerializeField] private AudioClip gameplaymusic;
    [SerializeField] private AudioClip rockcrush;

    private float fGameSound;
    public float GameSound
    {
        get { return fGameSound; }
        set { fGameSound = value; }
    }
    private float fMusic;
    public float Music
    {
        get { return fMusic; }
        set { fMusic = value; }
    }
    // Use this for initialization
    void Awake () {
        oInstance = this;
        aSource = GetComponent<AudioSource>();
        if (HighScoreHolder.hsInstance.PlayOnAwakeBool)
            PlayMenu2();
	}

    public void AdjustSound()
    {
        aSource.volume = fGameSound = HighScoreHolder.hsInstance.GameSound;
        aMusic.volume = fMusic = HighScoreHolder.hsInstance.Music;
    }

    public void PlayMenu1()
    {
        aSource.PlayOneShot(menu1);
    }

    public void PlayMenu2()
    {
        aSource.PlayOneShot(menu2);
    }

    public void PlayGunShot()
    {
        aSource.PlayOneShot(gunshot);
    }

    public void PlayEctoShot()
    {
        aSource.PlayOneShot(ectoshot);
    }
    
    public void PlayPause()
    {
        aSource.PlayOneShot(pause);
    }

    public void PlaySpawning()
    {
        aSource.PlayOneShot(waveque);
    }

    //public void PlayFriendShip()
    //{
    //    aMusic.clip = aFriendship;
    //    aMusic.Play();
    //}

    public void PlayCharacterDamage()
    {
        aSource.PlayOneShot(characterdamage[Random.Range(1, 4)]);
    }

    public void PlayZombieDamage()
    {
        aSource.PlayOneShot(zombiedamage[Random.Range(1, 4)]);
    }

    public void PlayZombieGroan()
    {
        aSource.PlayOneShot(zombiegroan[Random.Range(1, 4)]);
    }

    public void PlayHealPickUp()
    {
        aSource.PlayOneShot(healpickup);
    }

    public void MusicOptions(int nMus)
    {
        switch (nMus)
        {
            case 0:
                aMusic.clip = gameplaymusic;
                aMusic.Play();
                break;
            case 1:
                aMusic.Pause();
                break;
            case 2:
                aMusic.UnPause();
                break;
            case 3:
                aMusic.Stop();
                break;
        }
    }

    public void RockCrush()
    {
        aSource.PlayOneShot(rockcrush);
    }

    public void AdjustGS(float aud)
    {
        fGameSound = fGameSound + aud;
        aSource.volume = fGameSound;
        HighScoreHolder.hsInstance.GameSound = fGameSound;
    }
    public void AdjustMusic(float aud)
    {
        fMusic = fMusic + aud;
        aMusic.volume = fMusic;
        HighScoreHolder.hsInstance.Music = fMusic;
    }
}
