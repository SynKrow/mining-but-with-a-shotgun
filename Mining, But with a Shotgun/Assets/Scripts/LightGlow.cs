﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGlow : MonoBehaviour {

    private Light lGlow;

    private bool bGlow;


    private void Start()
    {
        lGlow = GetComponent<Light>();
        lGlow.range = Random.Range(2f, 5f);
    }

    // Update is called once per frame
    void Update () {
        if (!bGlow)
        {
            lGlow.range += 2 * Time.deltaTime;
            if(lGlow.range >= 5)
                bGlow = true;
        }
        if (bGlow)
        {
            lGlow.range -= 2 * Time.deltaTime;
            if(lGlow.range <= 2)
                bGlow = false;
        }
	}
}
