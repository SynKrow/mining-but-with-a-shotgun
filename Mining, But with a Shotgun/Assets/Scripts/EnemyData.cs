﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyData : MonoBehaviour
{

    private float fCurHealth;
    private float fMaxHealth = 3;

    private float fSpeed;

    [SerializeField] private GameObject gBody;
    [SerializeField] private Sprite[] sBody;
    [SerializeField] private GameObject gItem;

    private bool bDamagable = true;
    public bool Damagable
    {
        get { return bDamagable; }
        set { bDamagable = value; }
    }

    private Color32 cBase;


    private void DoubleSpeed()
    {
        fSpeed = fSpeed * 2;
        Invoke("DoubleSpeed", 5);
    }

    private void OnEnable()
    {
        gBody.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 150);
        fCurHealth = 0;
        fSpeed = 5;
        Invoke("DoubleSpeed", 25);
        EnemyManager.eManager.eClearEnemies += Deactivate;
        EnemyManager.eManager.eDisableSpeed += DisableSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        gBody.transform.eulerAngles = new Vector3(60, 0, 0);
        SetSprite();
        LookForPlayer();
        transform.Translate(0, 0, fSpeed * Time.deltaTime);
        GroanNoise();
    }

    private float fGroanNoise;
    private void GroanNoise()
    {
        fGroanNoise += Time.deltaTime;
        if(fGroanNoise >= Random.Range(5, 16))
        {
            if(Random.Range(0, 1.01f) > .3f)
            {
                OneShotPlaylist.oInstance.PlayZombieGroan();
            }
            fGroanNoise = 0;
        }
    }

    private void OnDisable()
    {
        CancelInvoke();
        EnemyManager.eManager.eClearEnemies -= Deactivate;
        EnemyManager.eManager.eDisableSpeed -= DisableSpeed;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void Damaged(int dmg)
    {
        OneShotPlaylist.oInstance.PlayZombieDamage();
        fCurHealth = fCurHealth + dmg;
        transform.Translate(0, 0, -.5f);
        if (fCurHealth >= fMaxHealth)
        {
            gameObject.SetActive(false);
            SpawnRock();
            GetGoo();
            EnemyManager.eManager.NEnemyNumber--;
            if(EnemyManager.eManager.NEnemyNumber <= 0)
                EnemyManager.eManager.SlowMoEffect();
        }
        else
        {
            StartCoroutine("DamagedFlash");
        }
    }

    private void SetSprite()
    {
        if(transform.position.x > PlayerData.pData.transform.position.x)
            gBody.GetComponent<SpriteRenderer>().sprite = sBody[0];
        else
            gBody.GetComponent<SpriteRenderer>().sprite = sBody[1];
    }

    private void LookForPlayer()
    {
        transform.LookAt(PlayerData.pData.transform.position);
    }

    IEnumerator DamagedFlash()
    {
        gBody.GetComponent<SpriteRenderer>().color = new Color32(255, 0, 0, 100);
        fSpeed = 0;
        yield return new WaitForSeconds(.35f);
        gBody.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 150);
        yield return new WaitForSeconds(.5f);
        fSpeed = 2.5f;
    }

    IEnumerator HealFlash()
    {
        gBody.GetComponent<SpriteRenderer>().color = new Color32(0, 255, 0, 150);
        yield return new WaitForSeconds(.35f);
        gBody.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 150);
    }

    public void Heal()
    {
        fCurHealth--;
        fSpeed++;
        gItem.SetActive(true);
        StartCoroutine("HealFlash");
    }

    public void DisableSpeed()
    {
        fSpeed = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerData>())
        {
            other.GetComponent<PlayerData>().Damaged();
            GetGoo();
            gameObject.SetActive(false);
            EnemyManager.eManager.NEnemyNumber--;
            if (EnemyManager.eManager.NEnemyNumber <= 0)
                EnemyManager.eManager.SlowMoEffect();
        }
    }

    private void SpawnRock()
    {
        GameObject gMiddleStone = RockPooler.rockPooler.GetRock();
        gMiddleStone.transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
        gMiddleStone.SetActive(true);
        for (int i = 0; i < Random.Range(3, 7); i++)
        {
            GameObject gNewStone = RockPooler.rockPooler.GetRock();
            gNewStone.transform.position = new Vector3(gMiddleStone.transform.position.x + Random.Range(-5, 5),
                gMiddleStone.transform.position.y,
                gMiddleStone.transform.position.z + Random.Range(-2, 2));
            gNewStone.SetActive(true);
        }
    }

    private void GetGoo()
    {
        GameObject gGoo = GooPooler.gooPooler.GetGoo();
        gGoo.transform.position = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
        gGoo.SetActive(true);
    }
}
