﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Falling : MonoBehaviour {

    [SerializeField] private GameObject gBody;
    private Vector3 startLoc;
    private float fSpeed;
    private float fRot;
    private bool bRot;

	// Use this for initialization
	void Start () {
        startLoc = transform.position;
        if (Random.Range(0f, 1f) > 0.5f)
            bRot = true;
        fSpeed = Random.Range(15, 25);
        fRot = Random.Range(25, 50);
	}
	
	// Update is called once per frame
	void Update () {
        if (bRot)
            gBody.transform.Rotate(0, 0, fRot * Time.deltaTime);
        else
            gBody.transform.Rotate(0, 0, -fRot * Time.deltaTime);
        transform.Translate(0, -fSpeed * Time.deltaTime, 0);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Wall>())
        {
            GetRock();
            transform.position = startLoc;
        }
    }

    private void GetRock()
    {
        GameObject gRock = GooPooler.gooPooler.GetRock();
        gRock.transform.position = transform.position;
        gRock.SetActive(true);
    }
}
