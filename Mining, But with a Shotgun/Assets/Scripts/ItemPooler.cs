﻿using System.Collections.Generic;
using UnityEngine;

public class ItemPooler : MonoBehaviour {

    public static ItemPooler itemPooler;
    [SerializeField] private GameObject gHealthPU;
    [SerializeField] private GameObject gAmmoPU;

    [SerializeField] private int nHealthAmount;
    [SerializeField] private int nAmmoAmount;


    [SerializeField] private bool bSpawn;
    private List<GameObject> lPooledHealth = new List<GameObject>();
    private List<GameObject> lPooledAmmo = new List<GameObject>();

    private void Awake()
    {
        itemPooler = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < nHealthAmount; i++)
        {
            GameObject gHealth = (GameObject)Instantiate(gHealthPU);
            lPooledHealth.Add(gHealth);
            gHealth.SetActive(false);
        }
        for (int i = 0; i < nAmmoAmount; i++)
        {
            GameObject gAmmo = (GameObject)Instantiate(gAmmoPU);
            lPooledAmmo.Add(gAmmo);
            gAmmo.SetActive(false);
        }
    }

    public GameObject GetHealth()
    {
        for (int i = 0; i < lPooledHealth.Count; i++)
        {
            if (!lPooledHealth[i].activeInHierarchy)
            {
                return lPooledHealth[i];
            }
        }
        if (bSpawn)
        {
            GameObject gAmmo = (GameObject)Instantiate(gHealthPU);
            lPooledHealth.Add(gAmmo);
            return gAmmo;
        }
        return null;
    }

    public GameObject GetAmmo()
    {
        for (int i = 0; i < lPooledAmmo.Count; i++)
        {
            if (!lPooledAmmo[i].activeInHierarchy)
            {
                return lPooledAmmo[i];
            }
        }
        if (bSpawn)
        {
            GameObject gAmmo = (GameObject)Instantiate(gAmmoPU);
            lPooledAmmo.Add(gAmmo);
            return gAmmo;
        }
        return null;
    }
}
