﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpacityTwo : MonoBehaviour {

    private SpriteRenderer self;
    private bool bOpacityOff;
    private float biteA;
    private float fTurnOff;

    private void Start()
    {
        self = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enabled)
        {
            transform.Translate(0, 2 * Time.deltaTime, 0);
            if (!bOpacityOff)
            {
                biteA += 250 * Time.deltaTime;
                self.color = new Color32(255, 255, 255, (byte)biteA);
                if(biteA >= 255)
                {
                    bOpacityOff = true;
                    self.color = new Color32(255, 255, 255, 255);
                }
            }
            else
            {
                biteA -= 250 * Time.deltaTime;
                self.color = new Color32(255, 255, 255, (byte)biteA);
                if(biteA <= 0)
                {
                    //PlayerData.pData.CreateMeteor();
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
