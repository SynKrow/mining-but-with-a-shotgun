﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunPellet : MonoBehaviour {

    [SerializeField] private GameObject[] gPellets;
    private float fDamage;
    private bool bEnhanced;
    private float fSpeed;
    public bool Enhanced
    {
        get { return bEnhanced; }
        set { bEnhanced = value; }
    }
    private Color32 cBase = new Color32(255, 255, 0, 255);
    private Color32 cCrit = new Color32(0, 255, 255, 255);

    // Use this for initialization
    void OnEnable () {
        Invoke("Deactivate", 1f);
        fSpeed = 50;
        for(int i = 0; i < gPellets.Length - 1; i++)
        {
            gPellets[i].transform.position = new Vector3(transform.position.x + Random.Range(-.25f, .25f), 
                transform.position.y + Random.Range(-.25f, .25f), 
                transform.position.z + Random.Range(-.25f, .25f));
            if (bEnhanced)
                gPellets[i].GetComponent<Renderer>().material.color = cCrit;
            gPellets[i].gameObject.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(0, 0, -fSpeed * Time.deltaTime);
	}

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyData>())
        {
            fSpeed = fSpeed / 2;
            if (bEnhanced)
            {
                other.GetComponent<EnemyData>().Damaged(3);
                Deactivate();
            }
            else
            {
                other.GetComponent<EnemyData>().Damaged(1);
                Invoke("Deactivate", .15f);
            }
        }
        else if (other.GetComponent<MainRock>())
        {
            fSpeed = fSpeed / 2;
            if (bEnhanced)
            {
                other.GetComponent<MainRock>().Damaged(3);
                Invoke("Deactivate", .15f);
            }
            else
            {
                other.GetComponent<MainRock>().Damaged(1);
                Deactivate();
            }
        }
        else if (other.GetComponent<Wall>())
            Deactivate();
    }

    private void OnDisable()
    {
        bEnhanced = false;
        for (int i = 0; i < gPellets.Length - 1; i++)
        {
            gPellets[i].SetActive(false);
            gPellets[i].GetComponent<Renderer>().material.color = cBase;
        }
        CancelInvoke();
    }

    
}
