﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPopUp : MonoBehaviour {

    private bool bFlash;
    private int nCounter;
    private SpriteRenderer sRend;

    private void Awake()
    {
        sRend = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        Invoke("DisableSelf", 0.15f);
    }

    private void DisableSelf()
    {
        if (nCounter < 5)
        {
            nCounter++;
            if (!bFlash)
                sRend.color = new Color32(255, 255, 255, 0);
            else
                sRend.color = new Color32(255, 255, 255, 255);
            bFlash = !bFlash;
            Invoke("DisableSelf", 0.15f);
        }
        else
        {
            nCounter = 0;
            bFlash = false;
            sRend.color = new Color32(255, 255, 255, 255);
            gameObject.SetActive(false);
        }
    }
}
