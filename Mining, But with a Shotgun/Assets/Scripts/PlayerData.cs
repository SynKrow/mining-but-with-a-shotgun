﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{

    public static PlayerData pData;
    private bool bActive;
    public bool Active
    {
        get { return bActive; }
        set { bActive = value; }
    }

    private float fMaxHealth = 5;
    private float fCurHealth;
    public float CurHealth
    {
        get { return fCurHealth; }
        set { fCurHealth = value; }
    }
    [SerializeField] private Image iHealth;
    private float fMaxAmmo = 30;
    private float fCurAmmo;
    public float CurAmmo
    {
        get { return fCurAmmo; }
        set { fCurAmmo = value; }
    }
    [SerializeField] private Image iAmmo;
    private float fSpeed = 10;
    [SerializeField] private Image iReload;
    [SerializeField] private GameObject gEndLoc;
    [SerializeField] private GameObject gShotLoc;
    [SerializeField] private GameObject gAnim;
    [SerializeField] private Image iHurt;
    private Animator aNim;
    private RaycastHit rHit;
    [SerializeField] private GameObject[] gPopups;
    //0 = Enemy Warning
    //1 = Health
    //2 = Plasma

    [SerializeField] private ParticleSystem pFlash;
    private bool bBuild;
    public bool Build
    {
        get { return bBuild; }
        set { bBuild = value; }
    }

    private AudioSource aSource;

    private bool bReloading;
    private float fReloadTimer;

    private int nDir = 1;

    private Vector3[] vKnockback = new Vector3[4]
    {
        new Vector3(0,0,-.75f),
        new Vector3(0,0,.75f),
        new Vector3(.75f,0,0),
        new Vector3(-.75f,0,0)
    };

    // 0 = back
    // 1 = forward
    // 2 = left
    // 3 = right

    // Use this for initialization
    void Start()
    {
        pData = this;
        fCurHealth = fMaxHealth;
        iHealth.fillAmount = fCurHealth / fMaxHealth;
        iAmmo.fillAmount = fCurAmmo / fMaxAmmo;
        aNim = gAnim.GetComponent<Animator>();
        Cursor.visible = false;
        aSource = GetComponent<AudioSource>();
    }

    public void ShowPopup(int array)
    {
        gPopups[array].SetActive(true);
    }

    public void Damaged()
    {
        fCurHealth--;
        iHealth.fillAmount = fCurHealth / fMaxHealth;
        OneShotPlaylist.oInstance.PlayCharacterDamage();
        switch (Mathf.RoundToInt(fCurHealth))
        {
            case 5:
                iHurt.color = new Color32(255, 255, 255, 0);
                fSpeed = 10.5f;
                break;
            case 4:
                iHurt.color = new Color32(255, 255, 255, 25);
                fSpeed = 10;
                break;
            case 3:
                iHurt.color = new Color32(255, 255, 255, 45);
                fSpeed = 9.5f;
                break;
            case 2:
                iHurt.color = new Color32(255, 255, 255, 65);
                fSpeed = 8.5f;
                break;
            case 1:
                iHurt.color = new Color32(255, 255, 255, 90);
                fSpeed = 7f;
                break;
        }
        if (fCurHealth <= 0)
        {
            EnemyManager.eManager.PlayerDeath();
            aSource.Stop();
        }
    }



    // Movement Code Start Here~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void MoveRight()
    {
        transform.Translate(fSpeed * Time.deltaTime, 0, 0);
        //gEndLoc.transform.position = new Vector3(transform.position.x + 5, transform.position.y, transform.position.z);
        //gShotLoc.transform.eulerAngles = new Vector3(0, -90, 0);
        //nDir = 3;
    }
    public void MoveLeft()
    {
        transform.Translate(-fSpeed * Time.deltaTime, 0, 0);
        //gEndLoc.transform.position = new Vector3(transform.position.x - 5, transform.position.y, transform.position.z);
        //gShotLoc.transform.eulerAngles = new Vector3(0, 90, 0);
        //nDir = 2;
    }
    public void MoveUp()
    {
        transform.Translate(0, 0, fSpeed * Time.deltaTime);
        //gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5);
        //gShotLoc.transform.eulerAngles = new Vector3(0, 180, 0);
        //nDir = 0;
    }
    public void MoveDown()
    {
        transform.Translate(0, 0, -fSpeed * Time.deltaTime);
        //gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5);
        //gShotLoc.transform.eulerAngles = new Vector3(0, 0, 0);
        //nDir = 1;
    }
    // Movement Code End Here~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    public void HealthPickUp()
    {
        ShowPopup(1);
        if (fCurHealth < fMaxHealth)
        {
            fCurHealth++;
            iHealth.fillAmount = fCurHealth / fMaxHealth;
            switch (Mathf.RoundToInt(fCurHealth))
            {
                case 5:
                    iHurt.color = new Color32(255, 255, 255, 0);
                    fSpeed = 10.5f;
                    break;
                case 4:
                    iHurt.color = new Color32(255, 255, 255, 25);
                    fSpeed = 10;
                    break;
                case 3:
                    iHurt.color = new Color32(255, 255, 255, 45);
                    fSpeed = 9.5f;
                    break;
                case 2:
                    iHurt.color = new Color32(255, 255, 255, 65);
                    fSpeed = 8.5f;
                    break;
                case 1:
                    iHurt.color = new Color32(255, 255, 255, 90);
                    fSpeed = 7f;
                    break;
            }
        }
    }
    // Meme, do not uncomment.
    //public void Update() 
    //{
    //    if (bFriendshipcannon)
    //    {
    //        gRotator.transform.Rotate(0, -60 * Time.deltaTime, 0);
    //        fFriendTimer -= Time.deltaTime;
    //        if (fFriendTimer <= 0)
    //        {
    //            if (nFriends != sFriends.Length)
    //            {
    //                sFriends[nFriends].gameObject.SetActive(true);
    //                nFriends++;
    //                fFriendTimer = 1;
    //            }
    //            if(nFriends2 == 0)
    //            {
    //                gPerfectBeing.SetActive(true);
    //                bFriendshipcannon = false;
    //            }
    //        }
    //    }
    //}

    public void AmmoPickUp()
    {
        ShowPopup(2);
        if (fCurAmmo < fMaxAmmo)
        {
            fCurAmmo = fCurAmmo + Random.Range(2, 5);
            if (fCurAmmo >= fMaxAmmo)
            {
                fCurAmmo = fMaxAmmo;
            }
            iAmmo.fillAmount = fCurAmmo / fMaxAmmo;
        }
    }

    public void ShootNormalPellets()
    {
        if (!bReloading)
        {
            pFlash.Play();
            SpawnPellet();
            transform.Translate(vKnockback[nDir]);
            bReloading = true;
        }
    }

    public void ShootEnhancedPellets()
    {
        if (!bReloading && fCurAmmo > 0)
        {
            pFlash.Play();
            SpawnEnhancedPellet();
            transform.Translate(vKnockback[nDir]);
            bReloading = true;
            fCurAmmo--;
            iAmmo.fillAmount = fCurAmmo / fMaxAmmo;
        }
    }

    private void SpawnPellet()
    {
        GameObject gPellet = BulletPooler.bulletPooler.GetPellet();
        gPellet.transform.position = gShotLoc.transform.position;
        gPellet.transform.rotation = gShotLoc.transform.rotation;
        gPellet.SetActive(true);
        OneShotPlaylist.oInstance.PlayGunShot();
    }

    private void SpawnEnhancedPellet()
    {
        GameObject gPellet = BulletPooler.bulletPooler.GetPellet();
        gPellet.transform.position = gShotLoc.transform.position;
        gPellet.transform.rotation = gShotLoc.transform.rotation;
        gPellet.GetComponent<ShotgunPellet>().Enhanced = true;
        gPellet.SetActive(true);
        OneShotPlaylist.oInstance.PlayEctoShot();
    }

    public void Reloading()
    {
        if (bReloading)
        {
            fReloadTimer -= Time.deltaTime;
            iReload.fillAmount = fReloadTimer / .5f;
            if (fReloadTimer <= 0)
            {
                fReloadTimer = .5f;
                bReloading = false;
            }
        }
    }

    //private bool bFriendshipcannon = false;
    //private bool bFriends2;
    //private int nFriends;
    //private int nFriends2;
    //public int NFriends2
    //{
    //    get { return nFriends2; }
    //    set { nFriends2 = value; }
    //}
    //private float fFriendTimer;
    //[SerializeField] private GameObject gRotator;
    //[SerializeField] private SpriteRenderer[] sFriends;
    //[SerializeField] private GameObject[] MeteorsInTheSky;
    //[SerializeField] private GameObject gPerfectBeing;
    //[SerializeField] private Image iScreen;
    //public void FriendShipCannon()
    //{
    //    if (fCurAmmo >= fMaxAmmo)
    //    {
    //        bFriendshipcannon = true;
    //        bFriends2 = true;
    //        nFriends2 = sFriends.Length;
    //        fFriendTimer = 1;
    //        bActive = false;
    //        OneShotPlaylist.oInstance.PlayFriendShip();
    //        EnemyManager.eManager.FriendshipCannon();
    //    }
    //}
    //public void CreateMeteor()
    //{
    //    for (int i = 0; i < MeteorsInTheSky.Length - 1; i++)
    //    {
    //        MeteorsInTheSky[i].SetActive(true);
    //    }
    //    InvokeRepeating("WhiteFade", 7, 0.01f);
    //}

    //private float fFade;
    //private void WhiteFade()
    //{
    //    fFade += 75 * Time.deltaTime;
    //    iScreen.color = new Color32(255, 255, 255, (byte)fFade);
    //    if(fFade >= 255)
    //    {
    //        Application.Quit();
    //        Debug.Log("Yes");
    //    }
    //}
    // To unlock meme, uncomment things in OpacityUpAndGo, OpacityTwo, and PlayerController
    private bool bForward = true;
    private bool bBack = true;
    private bool bLeft = true;
    private bool bRight = true;
    private int nHeld;
    public void AnimationController()
    {
        if (bForward)
        {
            if (Input.GetKey(KeyCode.S))// Move Forward Anim
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                aNim.SetBool("Front", true);
                aNim.SetBool("Back", false);
                aNim.SetBool("Left", false);
                aNim.SetBool("Right", false);
                aNim.SetBool("Move", true);
                bForward = false;
                nHeld++;
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 0, 0);
                nDir = 1;
                if (nHeld == 1)
                    aSource.Play();
            }
        }
        if (bBack)
        {
            if (Input.GetKey(KeyCode.W))// Move Back Anim
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                aNim.SetBool("Front", false);
                aNim.SetBool("Back", true);
                aNim.SetBool("Left", false);
                aNim.SetBool("Right", false);
                aNim.SetBool("Move", true);
                bBack = false;
                nHeld++;
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 180, 0);
                nDir = 0;
                if (nHeld == 1)
                    aSource.Play();
            }
        }
        if (bLeft)
        {
            if (Input.GetKey(KeyCode.A))// Move Left Anim
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                aNim.SetBool("Front", false);
                aNim.SetBool("Back", false);
                aNim.SetBool("Left", true);
                aNim.SetBool("Right", false);
                aNim.SetBool("Move", true);
                bLeft = false;
                nHeld++;
                gEndLoc.transform.position = new Vector3(transform.position.x - 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, 90, 0);
                nDir = 2;
                if (nHeld == 1)
                    aSource.Play();
            }
        }
        if (bRight)
        {
            if (Input.GetKey(KeyCode.D))// Move Right Anim
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                aNim.SetBool("Front", false);
                aNim.SetBool("Back", false);
                aNim.SetBool("Left", false);
                aNim.SetBool("Right", true);
                aNim.SetBool("Move", true);
                bRight = false;
                nHeld++;
                gEndLoc.transform.position = new Vector3(transform.position.x + 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, -90, 0);
                nDir = 3;
                if (nHeld == 1)
                    aSource.Play();
            }
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            if (!bBack)
            {
                aNim.SetBool("Back", true);
                aNim.SetBool("Front", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 180, 0);
                nDir = 0;
            }
            if (!bLeft)
            {
                aNim.SetBool("Left", true);
                aNim.SetBool("Front", false);
                gEndLoc.transform.position = new Vector3(transform.position.x - 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, 90, 0);
                nDir = 2;
            }
            if (!bRight)
            {
                aNim.SetBool("Right", true);
                aNim.SetBool("Front", false);
                gEndLoc.transform.position = new Vector3(transform.position.x + 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, -90, 0);
                nDir = 3;
            }
            nHeld--;
            if (nHeld == 0)
            {
                aNim.SetBool("Move", false);
                aSource.Stop();
            }
            bForward = true;
        }
        if (Input.GetKeyUp(KeyCode.W))// Anim End
        {
            if (!bForward)
            {
                aNim.SetBool("Front", true);
                aNim.SetBool("Back", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 0, 0);
                nDir = 1;
            }
            if (!bLeft)
            {
                aNim.SetBool("Left", true);
                aNim.SetBool("Back", false);
                gEndLoc.transform.position = new Vector3(transform.position.x - 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, 90, 0);
                nDir = 2;
            }
            if (!bRight)
            {
                aNim.SetBool("Right", true);
                aNim.SetBool("Back", false);
                gEndLoc.transform.position = new Vector3(transform.position.x + 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, -90, 0);
                nDir = 3;
            }
            nHeld--;
            if (nHeld == 0)
            {
                aNim.SetBool("Move", false);
                aSource.Stop();
            }
            bBack = true;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            if (!bBack)
            {
                aNim.SetBool("Back", true);
                aNim.SetBool("Left", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 180, 0);
                nDir = 0;
            }
            if (!bForward)
            {
                aNim.SetBool("Front", true);
                aNim.SetBool("Left", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 0, 0);
                nDir = 1;
            }
            if (!bRight)
            {
                aNim.SetBool("Right", true);
                aNim.SetBool("Left", false);
                gEndLoc.transform.position = new Vector3(transform.position.x + 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, -90, 0);
                nDir = 3;
            }
            nHeld--;
            if (nHeld == 0)
            {
                aNim.SetBool("Move", false);
                aSource.Stop();
            }
            bLeft = true;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            if (!bBack)
            {
                aNim.SetBool("Back", true);
                aNim.SetBool("Right", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 180, 0);
                nDir = 0;
            }
            if (!bLeft)
            {
                aNim.SetBool("Left", true);
                aNim.SetBool("Right", false);
                gEndLoc.transform.position = new Vector3(transform.position.x - 5, transform.position.y, transform.position.z);
                gShotLoc.transform.eulerAngles = new Vector3(0, 90, 0);
                nDir = 2;
            }
            if (!bForward)
            {
                aNim.SetBool("Front", true);
                aNim.SetBool("Right", false);
                gEndLoc.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 5);
                gShotLoc.transform.eulerAngles = new Vector3(0, 0, 0);
                nDir = 1;
            }
            nHeld--;
            if (nHeld == 0)
            {
                aNim.SetBool("Move", false);
                aSource.Stop();
            }
            bRight = true;
        }
    }
}
