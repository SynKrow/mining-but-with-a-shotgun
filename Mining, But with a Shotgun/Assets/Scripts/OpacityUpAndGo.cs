﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpacityUpAndGo : MonoBehaviour {

    private Image self;
    private float biteA;

    private void Start()
    {
        self = GetComponent<Image>();
    }

    private void OnEnable()
    {
        biteA = 255;
    }

    // Update is called once per frame
    void Update () {
        if (enabled)
        {
            biteA -= 350 * Time.deltaTime;
            self.color = new Color32(255, 255, 255, (byte)biteA);
            if(biteA < 5)
            {
                self.color = new Color32(255, 255, 255, 0);
                gameObject.SetActive(false);
            }
        }
	}
}
