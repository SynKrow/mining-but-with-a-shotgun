﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{

    public static EnemyManager eManager;

    public delegate void eDelegate();
    public event eDelegate eClearEnemies;
    public event eDelegate eDisableSpeed;


    [SerializeField] private Text tTime;
    private float fTime;

    [SerializeField] private GameObject[] gSpawnPoints;

    private int nMin = 1;
    private int nMax = 3;
    private int nCurTime = 5;

    private int nRandomEnemyAmount;
    private int nSpawnIndex;
    private float fTimeBetweenSpawn;
    private float fTimeBeforeSpawn = 2;
    private bool bSpawn;

    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject gPause;
    [SerializeField] private Text tFinalTime;
    [SerializeField] private Text tComment;
    [SerializeField] private GameObject gMainMenu;
    [SerializeField] private GameObject gOptions;
    [SerializeField] private GameObject gTutMenu;
    [SerializeField] private GameObject[] gTortureSlides;


    [SerializeField] private Image[] iMenu;
    private int nMenu;
    // 0 = Play
    // 1 = Controls
    // 2 = Options
    // 3 = Quit

    [SerializeField] private Image[] iRetry;

    [SerializeField] private Image[] iPaused;
    private int nPaused;
    //0 = Resume
    //1 = MainMenu

    [SerializeField] private Image[] iOptions;
    private int nOptions;
    //0 = Sound
    //1 = Music
    //2 = ShowTutorial

    //3 = Back

    [SerializeField] private Image[] iTutorial;
    private int nTutPanels;
    private int nTutorial;
    //0 = Next
    //1 = Skip
    //2 = StopShowing

    [SerializeField] private Text tHighScore;
    [SerializeField] private GameObject gNhs;
    [SerializeField] private GameObject gControls;
    [SerializeField] private Text tArrow;

    private bool bPaused = true;

    private string[] sComment = new string[10]
    {
        "How? It's not possible to lose this early!",
        "Did you even try?",
        "Is this the best you can really do?",
        "You need practice.",
        "The dev can do better than you.",
        "Not bad actually.",
        "Hey, nice score.",
        "You're pretty good at this game!",
        "I'm starting to get concerned...",
        "You need a break. Go outside, nerd."
    };
    private int nEnemyNumber;
    public int NEnemyNumber
    {
        get { return nEnemyNumber; }
        set { nEnemyNumber = value; }
    }

    private void Awake()
    {
        eManager = this;
        Time.timeScale = 0;
    }

    private void Start()
    {
        iMenu[nMenu].color = new Color32(100, 255, 255, 255);
        iMenu[nMenu].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        iMenu[nMenu].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
        iRetry[0].color = new Color32(100, 255, 255, 255);
        iRetry[0].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        iRetry[0].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
        iPaused[nPaused].color = new Color32(100, 255, 255, 255);
        iPaused[nPaused].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        iPaused[nPaused].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
        iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
        iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
        tHighScore.text = HighScoreHolder.hsInstance.HighScore.ToString();
        OneShotPlaylist.oInstance.AdjustSound();
        Screen.SetResolution(900, 900, false);
        gNhs.SetActive(false);
    }


    //public void FriendshipCannon()
    //{
    //    if(nEnemyNumber > 0)
    //        eDisableSpeed();
    //    bFunctionalUpdate = false;
    //    Debug.Log("Yes");
    //}

    public void PlayerDeath()
    {
        PlayerData.pData.Active = false;
        OneShotPlaylist.oInstance.MusicOptions(3);
        eClearEnemies();
        Time.timeScale = 0;
        tFinalTime.text = Mathf.RoundToInt(fTime).ToString();
        if (Mathf.RoundToInt(fTime) > HighScoreHolder.hsInstance.HighScore)
        {
            tHighScore.text = Mathf.RoundToInt(fTime).ToString();
            gNhs.SetActive(true);
        }
        HighScoreHolder.hsInstance.SetHighScore(Mathf.RoundToInt(fTime));
        HighScoreHolder.hsInstance.Saves();
        if (Mathf.RoundToInt(fTime) > 0 && Mathf.RoundToInt(fTime) < 10)
            tComment.text = sComment[0];
        else if (Mathf.RoundToInt(fTime) > 11 && Mathf.RoundToInt(fTime) < 100)
            tComment.text = sComment[1];
        else if (Mathf.RoundToInt(fTime) > 101 && Mathf.RoundToInt(fTime) < 500)
            tComment.text = sComment[2];
        else if (Mathf.RoundToInt(fTime) > 501 && Mathf.RoundToInt(fTime) < 1000)
            tComment.text = sComment[3];
        else if (Mathf.RoundToInt(fTime) > 1001 && Mathf.RoundToInt(fTime) < 2000)
            tComment.text = sComment[4];
        else if (Mathf.RoundToInt(fTime) > 2001 && Mathf.RoundToInt(fTime) < 3000)
            tComment.text = sComment[5];
        else if (Mathf.RoundToInt(fTime) > 3001 && Mathf.RoundToInt(fTime) < 4000)
            tComment.text = sComment[6];
        else if (Mathf.RoundToInt(fTime) > 4001 && Mathf.RoundToInt(fTime) < 5000)
            tComment.text = sComment[7];
        else if (Mathf.RoundToInt(fTime) > 5001 && Mathf.RoundToInt(fTime) < 7500)
            tComment.text = sComment[8];
        else if (Mathf.RoundToInt(fTime) > 7501 && Mathf.RoundToInt(fTime) < 10000)
            tComment.text = sComment[9];
        else if (Mathf.RoundToInt(fTime) > 10000)
            tComment.text = sComment[10];
        gameOverScreen.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        fTime += Time.deltaTime;
        tTime.text = Mathf.RoundToInt(fTime).ToString();
        if (!bSpawn)
            fTimeBeforeSpawn += Time.deltaTime;
        if (fTimeBeforeSpawn >= nCurTime)
        {
            gSpawnPoints[nSpawnIndex].GetComponent<Light>().color = new Color32(255, 0, 0, 255);
            OneShotPlaylist.oInstance.PlaySpawning();
            PlayerData.pData.ShowPopup(0);
            bSpawn = true;
            nRandomEnemyAmount = Random.Range(nMin, nMax + 1);
            nEnemyNumber = nRandomEnemyAmount + nEnemyNumber;
            nCurTime = nRandomEnemyAmount + 9;
            fTimeBeforeSpawn = 0;
        }
        if (bSpawn)
        {
            fTimeBetweenSpawn += Time.deltaTime;
            if (fTimeBetweenSpawn >= 1.5f)
            {
                SpawnEnemies();
                fTimeBetweenSpawn = 0;
            }
        }
        if (gMainMenu.activeInHierarchy && !gOptions.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (gControls.activeInHierarchy)
                    gControls.SetActive(false);
                iMenu[nMenu].color = new Color32(255, 255, 255, 255);
                iMenu[nMenu].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                iMenu[nMenu].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                nMenu++;
                if (nMenu > iMenu.Length - 1)
                    nMenu = 0;
                iMenu[nMenu].color = new Color32(100, 255, 255, 255);
                iMenu[nMenu].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                iMenu[nMenu].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (gControls.activeInHierarchy)
                    gControls.SetActive(false);
                iMenu[nMenu].color = new Color32(255, 255, 255, 255);
                iMenu[nMenu].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                iMenu[nMenu].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                nMenu--;
                if (nMenu < 0)
                    nMenu = iMenu.Length - 1;
                iMenu[nMenu].color = new Color32(100, 255, 255, 255);
                iMenu[nMenu].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                iMenu[nMenu].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                switch (nMenu)
                {
                    case 0:
                        if (HighScoreHolder.hsInstance.AutoSkip)
                        {
                            PlayerData.pData.Active = true;
                            gMainMenu.SetActive(false);
                            Time.timeScale = 1;
                            OneShotPlaylist.oInstance.MusicOptions(0);
                        }
                        else
                        {
                            gTutMenu.SetActive(true);
                            gMainMenu.SetActive(false);
                        }
                        break;
                    case 1:
                        if (!gControls.activeInHierarchy)
                            gControls.SetActive(true);
                        else
                            gControls.SetActive(false);
                        break;
                    case 2:
                        gOptions.SetActive(true);
                        nOptions = 0;
                        tArrow.transform.position = new Vector3(tArrow.transform.position.x, iOptions[nOptions].transform.position.y, tArrow.transform.position.z);
                        iOptions[0].fillAmount = OneShotPlaylist.oInstance.GameSound;
                        iOptions[1].fillAmount = OneShotPlaylist.oInstance.Music;
                        if (HighScoreHolder.hsInstance.AutoSkip)
                            iOptions[2].fillAmount = 1;
                        else
                            iOptions[2].fillAmount = 0;
                        break;
                    case 3:
                        Application.Quit();
                        break;
                }
                OneShotPlaylist.oInstance.PlayMenu2();
            }
        }
        if (gTutMenu.activeInHierarchy && !gMainMenu.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (nTutorial != 2)
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                    nTutorial = 2;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
                else
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    nTutorial = 0;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (nTutorial == 2)
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    nTutorial = 0;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
                else
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                    nTutorial--;
                    if (nTutorial < 0)
                        nTutorial = 1;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
            }
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (nTutorial == 2)
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    nTutorial = 0;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
                else
                {
                    iTutorial[nTutorial].color = new Color32(255, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                    nTutorial++;
                    if (nTutorial > 1)
                        nTutorial = 0;
                    iTutorial[nTutorial].color = new Color32(100, 255, 255, 255);
                    iTutorial[nTutorial].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    iTutorial[nTutorial].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                    OneShotPlaylist.oInstance.PlayMenu1();
                }
            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                switch (nTutorial)
                {
                    case 0:
                        if (nTutPanels >= gTortureSlides.Length - 1)
                        {
                            PlayerData.pData.Active = true;
                            gTutMenu.SetActive(false);
                            Time.timeScale = 1;
                            OneShotPlaylist.oInstance.MusicOptions(0);
                        }
                        else
                        {
                            if (nTutorial > 0)
                                gTortureSlides[nTutPanels].SetActive(false);
                            nTutPanels++;
                            gTortureSlides[nTutPanels].SetActive(true);
                            OneShotPlaylist.oInstance.PlayMenu1();
                        }
                        break;
                    case 1:
                        PlayerData.pData.Active = true;
                        gTutMenu.SetActive(false);
                        Time.timeScale = 1;
                        OneShotPlaylist.oInstance.MusicOptions(0);
                        break;
                    case 2:
                        if (!HighScoreHolder.hsInstance.AutoSkip)
                        {
                            HighScoreHolder.hsInstance.AutoSkip = true;
                            iTutorial[3].gameObject.SetActive(true);
                            OneShotPlaylist.oInstance.PlayMenu2();
                        }
                        else
                        {
                            HighScoreHolder.hsInstance.AutoSkip = false;
                            iTutorial[3].gameObject.SetActive(false);
                            OneShotPlaylist.oInstance.PlayMenu2();
                        }
                        break;
                }
            }
        }
        if (gOptions.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                nOptions++;
                if (nOptions > iOptions.Length - 1)
                    nOptions = 0;
                tArrow.transform.position = new Vector3(tArrow.transform.position.x, iOptions[nOptions].transform.position.y, tArrow.transform.position.z);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                nOptions--;
                if (nOptions < 0)
                    nOptions = iOptions.Length - 1;
                tArrow.transform.position = new Vector3(tArrow.transform.position.x, iOptions[nOptions].transform.position.y, tArrow.transform.position.z);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                switch (nOptions)
                {
                    case 0:
                        OneShotPlaylist.oInstance.AdjustGS(-.1f);
                        iOptions[nOptions].fillAmount = OneShotPlaylist.oInstance.GameSound;
                        OneShotPlaylist.oInstance.PlayMenu1();
                        break;
                    case 1:
                        OneShotPlaylist.oInstance.AdjustMusic(-.1f);
                        iOptions[nOptions].fillAmount = OneShotPlaylist.oInstance.Music;
                        break;
                }
            }
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                switch (nOptions)
                {
                    case 0:
                        OneShotPlaylist.oInstance.AdjustGS(.1f);
                        iOptions[nOptions].fillAmount = OneShotPlaylist.oInstance.GameSound;
                        OneShotPlaylist.oInstance.PlayMenu1();
                        break;
                    case 1:
                        OneShotPlaylist.oInstance.AdjustMusic(.1f);
                        iOptions[nOptions].fillAmount = OneShotPlaylist.oInstance.Music;
                        break;
                }
            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                switch (nOptions)
                {
                    case 2:
                        HighScoreHolder.hsInstance.AutoSkip = !HighScoreHolder.hsInstance.AutoSkip;
                        if (HighScoreHolder.hsInstance.AutoSkip)
                            iOptions[2].fillAmount = 1;
                        else
                            iOptions[2].fillAmount = 0;
                        OneShotPlaylist.oInstance.PlayMenu2();
                        break;
                    case 3:
                        HighScoreHolder.hsInstance.Saves();
                        OneShotPlaylist.oInstance.PlayMenu2();
                        gOptions.SetActive(false);
                        break;
                }
            }
        }
        if (gameOverScreen.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                OneShotPlaylist.oInstance.PlayMenu2();
            }
        }
        if (gPause.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                iPaused[nPaused].color = new Color32(255, 255, 255, 255);
                iPaused[nPaused].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                iPaused[nPaused].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                nPaused++;
                if (nPaused > iPaused.Length - 1)
                    nPaused = 0;
                iPaused[nPaused].color = new Color32(100, 255, 255, 255);
                iPaused[nPaused].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                iPaused[nPaused].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                iPaused[nPaused].color = new Color32(255, 255, 255, 255);
                iPaused[nPaused].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
                iPaused[nPaused].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 0);
                nPaused--;
                if (nPaused < 0)
                    nPaused = iPaused.Length - 1;
                iPaused[nPaused].color = new Color32(100, 255, 255, 255);
                iPaused[nPaused].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
                iPaused[nPaused].GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, 7.5f);
                OneShotPlaylist.oInstance.PlayMenu1();
            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                switch (nPaused)
                {
                    case 0:
                        PlayerData.pData.Active = true;
                        bPaused = true;
                        gPause.SetActive(false);
                        Time.timeScale = 1;
                        OneShotPlaylist.oInstance.MusicOptions(2);
                        break;
                    case 1:
                        if (Mathf.RoundToInt(fTime) > HighScoreHolder.hsInstance.HighScore)
                            tHighScore.text = Mathf.RoundToInt(fTime).ToString();
                        HighScoreHolder.hsInstance.SetHighScore(Mathf.RoundToInt(fTime));
                        HighScoreHolder.hsInstance.Saves();
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                        break;
                }
                OneShotPlaylist.oInstance.PlayMenu2();
            }
        }
        if (!gTutMenu.activeInHierarchy && !gMainMenu.activeInHierarchy && !gameOverScreen.activeInHierarchy && !gPause.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
        {
            OneShotPlaylist.oInstance.PlayPause();
            PlayerData.pData.Active = false;
            OneShotPlaylist.oInstance.MusicOptions(1);
            gPause.SetActive(true);
            bPaused = !bPaused;
            if (!bPaused)
                Time.timeScale = 0;
            else if (bPaused)
            {
                Time.timeScale = 1;
                OneShotPlaylist.oInstance.MusicOptions(2);
                gPause.SetActive(false);
            }
        }
    }
    private int nCurEnemy;
    private void SpawnEnemies()
    {
        if (nCurEnemy < nRandomEnemyAmount)
        {
            gSpawnPoints[nSpawnIndex].GetComponent<Light>().color = new Color32(58, 182, 255, 255);
            GameObject gSpawned = EnemyPooler.enemyPooler.GetEnemy();
            gSpawned.transform.position = gSpawnPoints[nSpawnIndex].transform.position;
            gSpawned.SetActive(true);
            nCurEnemy++;
            if(nCurEnemy < nRandomEnemyAmount)
            {
                nSpawnIndex = Random.Range(0, gSpawnPoints.Length);
                gSpawnPoints[nSpawnIndex].GetComponent<Light>().color = new Color32(255, 0, 0, 255);
            }
        }
        else
        {
            gSpawnPoints[nSpawnIndex].GetComponent<Light>().color = new Color32(58, 182, 255, 255);
            bSpawn = false;
            fTimeBetweenSpawn = 0;
            nCurEnemy = 0;
            nMin++;
            nMax++;
            nSpawnIndex = Random.Range(0, gSpawnPoints.Length);
            if (nMax > 8)
            {
                GetComponent<RockPooler>().IncreaseRate();
            }
        }
    }

    public void SlowMoEffect()
    {
        Time.timeScale = 0.2f;
        InvokeRepeating("SlowMo", 0, .01f);
    }

    private void SlowMo()
    {
        Time.timeScale += Time.deltaTime;
        if (Time.timeScale >= 1)
        {
            Time.timeScale = 1;
            CancelInvoke();
        }
    }

    public void OnClick(int func)
    {
        switch (func)
        {
            case 0: // Play
                gMainMenu.SetActive(false);
                Time.timeScale = 1;
                break;
            case 1: // Retry
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            case 2: // Quit
                Application.Quit();
                break;
        }
    }
}
