﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private PlayerData pData;

	// Use this for initialization
	void Start () {
        pData = GetComponent<PlayerData>();
	}


	// Update is called once per frame
	void Update () {
        if (pData.Active)
        {
            pData.Reloading();
            pData.AnimationController();
            if (Input.GetKeyDown(KeyCode.J))
                pData.ShootNormalPellets();
            if (Input.GetKeyDown(KeyCode.K))
                pData.ShootEnhancedPellets();
            //if (Input.GetKeyDown(KeyCode.F))
            //    pData.FriendShipCannon();
            if (Input.GetKey(KeyCode.W))
                pData.MoveUp();
            if (Input.GetKey(KeyCode.A))
                pData.MoveLeft();
            if (Input.GetKey(KeyCode.S))
                pData.MoveDown();
            if (Input.GetKey(KeyCode.D))
                pData.MoveRight();
        }
    }
}
