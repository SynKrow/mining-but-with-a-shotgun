﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooler : MonoBehaviour {

    public static EnemyPooler enemyPooler;
    [SerializeField] private GameObject gEnemy;
    [SerializeField] private int nAmount;


    [SerializeField] private bool bSpawn;
    private List<GameObject> lPooledEnemy = new List<GameObject>();

    private void Awake()
    {
        enemyPooler = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < nAmount; i++)
        {
            GameObject gSpawnedEnemy = (GameObject)Instantiate(gEnemy);
            lPooledEnemy.Add(gSpawnedEnemy);
            gSpawnedEnemy.SetActive(false);
        }
    }

    public GameObject GetEnemy()
    {
        for (int i = 0; i < lPooledEnemy.Count; i++)
        {
            if (!lPooledEnemy[i].activeInHierarchy)
            {
                return lPooledEnemy[i];
            }
        }
        if (bSpawn)
        {
            GameObject gSpawnedEnemy = (GameObject)Instantiate(gEnemy);
            lPooledEnemy.Add(gSpawnedEnemy);
            return gSpawnedEnemy;
        }
        return null;
    }
}
